# README #

Teste para developer full-stack R4YOU.

### Neste repositório: ###

* Wordpress sem customizações versão 5.7
* Dois Logos R4YOU (versão preta e versão branca)

### Teste ###

* Criar um tema responsivo Wordpress customizado de sua preferência.
* Para a página interna (notícias/posts) seguir o layout das imagens layout_interna_teste.png e layout_interna_teste_ex.png disponíveis neste repositório.
* * Para marcação de anúncios no meio do conteúdo utilize imagens geradas pelo https://placeholder.com/, sendo elas https://via.placeholder.com/728x90 para desktop e https://via.placeholder.com/320x100 para mobile. O Post deverá ter pelo menos 3 repetições dessas imagens no meio do seu conteúdo.
* Não é necessário se atentar a margens exatas do layout e detalhes do layout podem ficar a critério do candidato.
* As outras páginas são de layout livre.
* Usar logo da R4YOU (escolher qualquer um dos dois disponíveis: logo-r4you-branco.png e logo-r4you-final.png).
* Favor, em todos os arquivos criados para o tema, incluir documentação.
* Incluir plugins que achar necessário e criar um arquivo para explicar o porquê da inclusão do plugin.
* Não esquecer de enviar dump do banco de dados para teste local.

### Envio ###

* Favor, postar em uma conta git e enviar link por email.
* Em caso de problema, enviar o código zippado por email.

### Objetivo ###

* O objetivo deste teste é analisar o conhecimento do candidato da estrutura do CMS Wordpress e sua familiaridade com programação web.